const { Schema } = require("mongoose");
const { ObjectID } = require('mongodb');

const Tariff = new Schema({
    _id: ObjectID,
    name: String,//title
    price: Number,//pages
    brand: String,//brand
    consumers: Number//author
})

module.exports = Tariff;