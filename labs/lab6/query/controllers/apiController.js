const config = require("../config");
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const Tariff = require("../models/tariff");
const Vote = require("../models/vote");
const User = require("../models/user");

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
const dbUrl = config.url;

function checkToken(token)
{
    return jwt.verify(token , config.jwtSecret , (err , verifiedJwt) => {
        if(err)
        {
            return 401;
        }
        else
        {
            return true;
        }
    });
}

function unpackToken(token)
{
    return jwt.decode(token);
}

async function getUser(user) {
    const model = mongoose.model("users", User);
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({ "login": user }) }).catch((error) => { return 500 });
}

async function getTariffs() {
    const model = mongoose.model("tariffs", Tariff);
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({}) }).catch((error) => { return 500 });
}

async function getVotesByTariffId(tariffId) {
    const model = mongoose.model("votes", Vote);
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({ "tariff_id": tariffId }).sort({ "time": "desc" }) }).catch((error) => { return 500 });
}

async function createQueryEntities() {
    const tariffsVotes = new Array();
    const tariffs = await getTariffs();
    for (i = 0; i < tariffs.length; i++) {
        const result = {
            tariff_id: tariffs[i]._id,
            name: tariffs[i].name,
            votes: null
        }
        const votes = await getVotesByTariffId(tariffs[i]._id);
        if (votes == 500 || votes.length == 0) {
            tariffsVotes.push(result);
            continue;
        } else {
            const result_votes = new Array();
            for (j = 0; j < votes.length; j++) {
                const user = await getUser(votes[j].user);
                const voteUser = {
                    user_id: user[0]._id,
                    login: user[0].login
                }
                const vote = {
                    time: votes[j].time,
                    user: voteUser
                }
                result_votes.push(vote);
            }
            result.votes = result_votes;
        }
        tariffsVotes.push(result);
    }
    return tariffsVotes;
}

module.exports = {
    async tariffsVotes(req, res) {
        if (req.query.page == null || req.query.size == null) {
            res.status(400).send("Bad request");
            return;
        }
        const page = parseInt(req.query.page);
        const amount = parseInt(req.query.size);
        const offset = (page - 1) * amount;
        const tariffsVotes = await createQueryEntities();
        const new_tariffsVotes = tariffsVotes.slice(offset, offset + amount);
        res.status(200).json(new_tariffsVotes);
    },

    async tariffsByIdVotes(req, res) {
        if (req.params.id == null) {
            res.status(400).send("Bad request");
            return;
        }
        const tariffId = req.params.id;
        const tariffsVotes = await createQueryEntities();
        for (i = 0; i < tariffsVotes.length; i++) {
            if (tariffsVotes[i].tariff_id == tariffId) {
                res.status(200).json(tariffsVotes[i]);
                return;
            }
        }
        res.status(404).send("Not found");
    },

    async userByIdVotes(req, res) {
        if (req.query.page == null || req.query.size == null) {
            res.status(400).send("Bad request");
            return;
        }
        if (req.params.id == null) {
            res.status(400).send("Bad request");
            return;
        }
        const userId = req.params.id;
        const page = parseInt(req.query.page);
        const amount = parseInt(req.query.size);
        const offset = (page - 1) * amount;
        const tariffsVotes = await createQueryEntities();
        const result = new Array();
        for (i = 0; i < tariffsVotes.length; i++) {
            if (tariffsVotes[i].votes == null) {
                continue;
            }
            for (j = 0; j < tariffsVotes[i].votes.length; j++) {
                if (tariffsVotes[i].votes[j].user.user_id == userId) {
                    result.push(tariffsVotes[i]);
                }
            }
        }
        const new_result = result.slice(offset, offset + amount);
        res.status(200).json(new_result);;
    },

    async meVotes(req , res)
    {
        if (req.query.page == null || req.query.size == null) {
            res.status(400).send("Bad request");
            return;
        }
        if(req.query == null)
        {
            res.status(400).send("Error");
            return;
        }
        /*if(req.query.token == null)
        {
            res.status(400).send("Incorrect input");
            return;
        }
        const token = req.query.token;
        const verify = checkToken(token);
        if(verify == 401)
        {
            res.status(401).send("Incorrect token");
            return;
        }
        const user = unpackToken(token);*/
        const fullUser = await getUser(user.login);
        console.log(fullUser);
        const page = parseInt(req.query.page);
        const amount = parseInt(req.query.size);
        const offset = (page - 1) * amount;
        const tariffsVotes = await createQueryEntities();
        const result = new Array();
        for (i = 0; i < tariffsVotes.length; i++) {
            if (tariffsVotes[i].votes == null) {
                continue;
            }
            for (j = 0; j < tariffsVotes[i].votes.length; j++) {
                if (tariffsVotes[i].votes[j].user.user_id.toString() == fullUser[0]._id.toString()) {
                    result.push(tariffsVotes[i]);
                }
            }
        }
        const new_result = result.slice(offset, offset + amount);
        res.status(200).json(new_result);;
    }
}