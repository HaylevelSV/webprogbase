const { json } = require('body-parser');
const config = require("../config");
const mongoose = require('mongoose');
const { ObjectID } = require("bson");
const jwt = require("jsonwebtoken");
const Tariff = require("../models/tariff");

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
const dbUrl = config.url;
const model = mongoose.model("tariffs" , Tariff);

async function addTariff(tariff)
{
    return mongoose.connect(dbUrl, connectOptions).then(() => model.insertMany(new model(tariff))).catch((error) => { res.status(500); return; });  
}

async function updateTariff(tariff)
{
    return mongoose.connect(dbUrl , connectOptions).then(() => {return model.find({})}).then(() => {return model.updateOne({"_id": tariff._id} , tariff)}).catch((error)=> {return 500;})
}

async function deleteTariff(tariff)
{
    return mongoose.connect(dbUrl , connectOptions).then(() => model.find({})).then(() => {return model.remove({_id: tariff._id})}).catch((error)=> {return 500});
}

module.exports = {
    async tariffCRUD(message)
    {
        switch(message.type) {
            case 'add':
                delete message.type;
                await addTariff(message);
                break;
            case 'update':
                delete message.type;
                await updateTariff(message);
                break; 
            case 'delete':
                delete message.type;
                await deleteTariff(message);
                break; 
            default:
                console.log("Error with tariff messages"); 
                break;
        }
    }
}