const router = require('express').Router();
const apiController = require('../controllers/apiController');
const bodyparser = require('body-parser');
const { get } = require('http');

router.use(bodyparser.json());
router.use(bodyparser.urlencoded({ extended: true }));

router
.get('/tariffs/votes' , apiController.tariffsVotes)
.get('/tariffs/:id/votes' , apiController.tariffsByIdVotes)
.get('/user/:id/votes' , apiController.userByIdVotes)
.get('/me/votes' , apiController.meVotes)

module.exports = router;