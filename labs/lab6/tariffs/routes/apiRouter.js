const router = require('express').Router();
const apiController = require('../controllers/apiController');
const bodyparser = require('body-parser');
const { get } = require('http');

router.use(bodyparser.json());
router.use(bodyparser.urlencoded({ extended: true }));

router
.get("/tariffs/:tariffId" , apiController.getTariffById)
.get("/tariffs" , apiController.getTariffs)
.post("/tariffs" , apiController.addTariff)
.put("/tariffs" , apiController.updateTariff)
.post("/tariffs" , apiController.deleteTariff)
module.exports = router;