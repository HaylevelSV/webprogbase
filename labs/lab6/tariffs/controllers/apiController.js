const Tariff = require("../models/tariff");
const config = require("../config");
const { json } = require('body-parser');
const mongoose = require('mongoose');
const { ObjectID } = require("bson");
const jwt = require("jsonwebtoken");
const amqp = require('amqplib');

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
const dbUrl = config.url;
const model = mongoose.model("tariffs", Tariff);

function checkToken(token) {
    return jwt.verify(token, config.jwtSecret, (err, verifiedJwt) => {
        if (err) {
            return 401;
        }
        else {
            return true;
        }
    });
}

function unpackToken(token) {
    return jwt.decode(token);
}

async function getTariffById(tariffId) {
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({ "_id": tariffId }) }).catch((error) => { return 500 });
}

async function getAllTariffs() {
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({}) }).catch((error) => { return 500 });
}

async function updateTariffById(tariff)
{
    return mongoose.connect(dbUrl , connectOptions).then(() => {return model.find({})}).then(() => {return model.updateOne({"_id": tariff._id} , tariff)}).catch((error)=> {return 500;})
}

module.exports = {
    async getTariffById(req, res) {
        const tariffId = req.params.tariffId;
        const tariff = await getTariffById(tariffId);
        if (tariff == 500 || tariff.length == 0) {
            res.status(500).send("Error");
        }
        res.status(200).json(tariff[0]);
    },

    async getTariffs(req, res) {
        if (req.query.page == null || req.query.size == null) {
            res.status(400).send("Bad request");
        } else {
            const page = parseInt(req.query.page);
            const amount = parseInt(req.query.size);
            const offset = (page - 1) * amount;
            const tariffs = await getAllTariffs();
            const new_tariffs = tariffs.slice(offset, offset + amount);
            res.status(200).json(new_tariffs);
        }
    },

    async addTariff(req, res) {
        if (req.body == null) {
            console.log(69);
            res.status(400).send("Error");
            return;
        }
        if (req.body.name == null || req.body.price == null || req.body.consumers == null) {
            console.log(74);
            res.status(400).send("Incorrect input");
            return;
        }
        /*const token = req.body.token;
        if (token == null && token == undefined) {
            console.log(80);
            res.status(401).send("Unauthorized!");
            return;
        }
        const check = checkToken(token);
        if (check == 401) {
            console.log(86);
            res.status(401).send("Unauthorized");
            return;
        }
        const author = unpackToken(token);*/
        const tariff = {
            _id: new ObjectID(),
            name: req.body.name,
            price: req.body.price,
            consumers: req.body.consumers,
            brand: author.brand
        }
        mongoose.connect(dbUrl, connectOptions).then(() => model.insertMany(new model(tariff))).catch((error) => { res.status(500); return; });
        res.status(201);
        res.json(tariff);
        tariff.type = 'add';
        amqp.connect(config.rabbitmq)
            .then(connection => {
                return connection.createChannel();
            })
            .then(channel => {
                const queue = 'tariffs';
                channel.assertQueue(queue);
                channel.sendToQueue(queue , Buffer.from(JSON.stringify(tariff)));
            }).catch(err => console.log('amqp' , err));
    },

    async updateTariff(req, res) {
        if (req.body == null) {
            res.status(400).send("Error");
            return;
        }
        if (req.body._id == null || req.body.name == null || req.body.speed == null || req.body.damage == null) {
            res.status(400).send("Incorrect input");
            return;
        }
        /*const token = req.body.token;
        if (token == null && token == undefined) {
            res.status(401).send("Unauthorized!");
            return;
        }
        const check = checkToken(token);
        if (check == 401) {
            res.status(401).send("Unauthorized");
            return;
        }
        const author = unpackToken(token);*/
        const tariff = await getTariffById(req.body._id);
        if(tariff == 500 || tariff.length == 0)
        {
            res.status(500).send("DB error");
            return;
        }
        if (tariff[0].author != author.login) {
            res.status(403).send("Forbidden operation");
            return;
        }
        const update_tariff = {
            _id: req.body._id,
            name: req.body.name,
            price: req.body.price,
            consumers: req.body.consumers,
            brand: author.brand
        }
        await updateTariffById(update_tariff);
        res.status(200);
        res.json(update_tariff);
        update_tariff.type = 'update';
        amqp.connect(config.rabbitmq)
            .then(connection => {
                return connection.createChannel();
            })
            .then(channel => {
                const queue = 'tariffs';
                channel.assertQueue(queue);
                channel.sendToQueue(queue , Buffer.from(JSON.stringify(update_tariff)));
            }).catch(err => console.log('amqp' , err));
    },

    async deleteTariff(req , res)
    {
        if (req.body == null) {
            res.status(400).send("Error");
            return;
        }
        /*const token = req.body.token;
        if (token == null && token == undefined) {
            res.status(401).send("Unauthorized!");
            return;
        }
        const check = checkToken(token);
        if (check == 401) {
            res.status(401).send("Unauthorized");
            return;
        }
        const author = unpackToken(token);*/
        var tariff = await getTariffById(req.body._id);
        if (tariff == 500 || tariff.length == 0) {
            res.status(500).send("Error");
            return;
        }
        if(tariff[0].author != author.login)
        {
            res.status(403).send("Forbidden operation");
            return;
        }
        mongoose.connect(dbUrl , connectOptions).then(() => model.find({})).then(() => {return model.remove({_id: req.body._id})}).catch((error)=> {return 500});
        res.status(200).json(tariff[0]);
        tariff = tariff[0];
        const queue_tariff = {
            _id: tariff._id,
            name: tariff.name,
            speed: tariff.speed,
            damage: tariff.damage,
            author: tariff.author,
            type: 'delete'
        }
        amqp.connect(config.rabbitmq)
            .then(connection => {
                return connection.createChannel();
            })
            .then(channel => {
                const queue = 'tariffs';
                channel.assertQueue(queue);
                channel.sendToQueue(queue , Buffer.from(JSON.stringify(queue_tariff)));
            }).catch(err => console.log('amqp' , err));
    }
}