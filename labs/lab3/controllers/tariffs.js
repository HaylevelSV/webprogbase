const site_rep = require('../repositories/TariffRepository.js');
const media_rep = require('../repositories/mediaRepository.js');
const e = require('express');
const tariffRepository = new site_rep("./data/tariffs.json");
const mediaRepository = new media_rep();
module.exports = {
    getTariffs(req, res) {
        let page = Number.parseInt(req.param("page") || 1);
        let records = Number.parseInt(req.param("per_page") || 5);
        let name = req.param("name") || "";
        if (!Number.isInteger(page) || !Number.isInteger(records)) {
            res.statusCode = 404;
            let arr = []
            res.render('tariffs', { arr });
        } else {
            res.statusCode = 200;
            let arr = tariffRepository.getTariffs();
            if (name !== undefined) {
                arr = arr.filter(el => el.fullname.startsWith(name));
            }
            let filtered = [];
            filtered = arr.concat(filtered);
            let record_count = arr.length;
            let page_count = Math.ceil(record_count / records);
            let left = records * (page - 1);
            let right = records * page
            let current = page;
            let next = page + 1;
            let prev = page - 1;
            let currnall = `${page}/${page_count}`;
            arr = arr.slice(left, right);
            let str = '';
            let start = name;
            if (filtered.length === 0) {
                str = `Тарифи, название которых начинаются с ${name}, не найдены`;
            } else {
                str = `Тарифи, название которых начинаются с ${name}`;
            }
            if (page < 1) {
                arr = [];
                prev = 0;
                next = 1;
                res.render('tariffs', { arr, str, current, next, prev, currnall, start, name });
            } else if (page > page_count) {
                arr = [];
                next = page_count + 1;
                prev = page_count;
                res.render('tariffs', { arr, str, current, next, prev, currnall, start, name });
            } else {
                res.render('tariffs', { arr, str, current, next, prev, currnall, start, name });
            }
        }
    },
    getTariffById(req, res) {
        res.statusCode = 200;
        const tariff = tariffRepository.getTariffsById(req.params.id)
        res.render('tariff', { tariff });
    },
    addTariff(req, res) {
        if (req.body.fullname === undefined || req.body.brand === undefined) {
            res.set("Content-Type", "application/json");
            res.statusCode = 400;
            res.send(JSON.stringify('Bad request'));
        } else {
            res.statusCode = 201;
            mediaRepository.addMedia(req.files);
            let site = req.body;
            console.log(__dirname);
            site.url = `/data/media/${req.files.photoFile.name}`;
            const id = tariffRepository.addTariff(site);
            const tariff = tariffRepository.getTariffsById(id);
            res.redirect(`/tariffs/${id}`);
        }
    },
    updateTariff(req, res) {
        res.set("Content-Type", "application/json");
        const tariff = tariffRepository.updateTariff(req.body)
        if (tariff === null) {
            res.statusCode = 404;
            res.send(`Any tariff wasnt updated`);
        } else if (req.body.name === undefined || req.body.address === undefined) {
            res.statusCode = 400;
            res.send(JSON.stringify('Bad request'));
        } else {
            res.send(`Updated tariff: ${JSON.stringify(tariff,null,4)}`);
        }
    },
    deleteTariff(req, res) {
        res.statusCode = 200;
        const tariff = tariffRepository.deleteTariffs(req.params.id)
        res.redirect(`/tariffs`);
    },
    createTariff(req, res) {
        res.render('new');
    }
};