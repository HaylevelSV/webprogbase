const fs = require('fs');

class JsonStorage {

    constructor(filePath) {
        this.filePath = filePath;
    }
    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const items = JSON.parse(jsonText);
        return items;
    }
    writeItems(items) {
        const jsonItem = JSON.stringify(items, null, 4);
        fs.writeFileSync(this.filePath, jsonItem);
    }
};


module.exports = JsonStorage;