const JsonStorage = require('../jsonStorage');
const Tariff = require('../models/tariff');
class UserRepository {

    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    addTariff(entityModel) {
        let tariffs = this.storage.readItems();
        const nextId = tariffs.nextId
        entityModel.id = nextId;
        tariffs.nextId++;
        tariffs.items.push(entityModel);
        this.storage.writeItems(tariffs);
        return entityModel.id;
    }
    getTariffs() {
        const items = this.storage.readItems().items;
        const arr = [];
        for (let i of items) {
            arr.push(new Tariff(i.id, i.brand, i.fullname, i.price, i.registeredAt, i.consumers))
        }
        return arr;
    }
    getTariffsById(entityId) {
        entityId = parseInt(entityId);
        const items = this.storage.readItems().items;
        for (let i of items) {
            if (entityId === i.id) {
                return new Tariff(i.id, i.brand, i.fullname, i.price, i.registeredAt, i.consumers);
            }
        }
        return null;
    }
    updateTariff(entityModel) {
        let tariffs = this.storage.readItems();
        for (let i = 0; i < tariffs.items.length; i++) {
            if (entityModel.id == tariffs.items[i].id) {
                tariffs.items[i] = entityModel;
                this.storage.writeItems(tariffs);
                return tariffs.items[i];
            }
        }
        return null;
    }
    deleteTariffs(entityId) {
        entityId = parseInt(entityId);
        let tariffs = this.storage.readItems();
        for (let i = 0; i < tariffs.items.length; i++) {
            if (entityId === tariffs.items[i].id) {
                const deletedTariff = tariffs.items[i];
                tariffs.items.splice(i, i + 1);
                tariffs.nextId--;
                this.storage.writeItems(tariffs);
                return deletedTariff;
            }
        }
        return null;
    }
};


module.exports = UserRepository;